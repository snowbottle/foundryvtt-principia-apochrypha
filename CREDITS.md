# book credits

- Written by David Perry, Ben Milton and Steven Lumpkin
- Illustrated by Evlyn Moreau

original site for the book: https://lithyscaphe.blogspot.com/p/principia-apocrypha.html

licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License:
https://creativecommons.org/licenses/by-nc-sa/4.0/
