# About this module
This is the *Principia Apochrypha*, a free book on running TTRPGs in the old-school style. It has been converted to Foundry VTT JournalEntry format.

Official site for the book: https://lithyscaphe.blogspot.com/p/principia-apocrypha.html

# Creators
- Written by David Perry, Ben Milton and Steven Lumpkin
- Illustrated by Evlyn Moreau

# License
Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License:
https://creativecommons.org/licenses/by-nc-sa/4.0/
