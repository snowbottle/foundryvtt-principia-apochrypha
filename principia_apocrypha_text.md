# PRINCIPIA APOCRYPHA
Elementary Axioms & Aphorisms On Running & Playing Tabletop RPGs In The Old School Style From Ben Milton & Steven Lumpkin
Assembled & Ammended By David Perry
Illustrated & Illuminated By Evlyn Moreau

lithyscaphe.blogspot.com/p/principia-apocrypha.html

Many thanks to G+ friends, the RPG Talk Discord, and the rest of the Gygaxian Democracy for their wisdom, proofing, and copyediting
A special thanks to Evlyn Moreau & Eric Nieudan
Layout by David Perry via Scribus
Fonts used are ITC Souvenir & Lora

This work may be freely distributed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License

# Attribution Key
(M) Ben Milton
Game designer of Knave and reviewer
at Questing Beast: questingblog.com
Original principle text from Maze Rats:
drivethrurpg.com/product/197158

(L) Steven Lumpkin
GM of Rollplay: The West Marches
Original principle text from
Agendas for Old School Gaming posts:
roll1d100.blogspot.com

(P) David Perry
Humble additions from
a neophyte syncretic GM
lithyscaphe.blogspot.com

Illustrations by Evlyn Moreau
Artist and creator of Chromatic Soup
chaudronchromatique.blogspot.com


# Introduction

## Rediscovering the context of old school gaming
Players and GMs newly discovering the old school style of RPG play might lack the historical context that rules designers sometimes take for granted as implicit. Many rules-light games in the old school style don’t include much guidance on running the game. I imagine newcomers might feel quite lost, or have a bad experience running or playing them.

So I decided to compile some advice, borrowing the effective, evocative style of "GM Principles" used by Apocalypse World and other Powered by the Apocalypse games.

These principles are primarily from two sources that are excellent in their own right: Maze Rats, a rules-light, old school style RPG by Ben Milton, and some thoughtful posts and vlogs by Steven Lumpkin, on his blog Roll 1d100. These sources are attributed using the symbols noted in the frontmatter. I’ve taken some liberty to edit them into a more cohesive whole, given them concise and salient titles, as well as expounding on them and adding a few principles myself.

## What is old school style play?
A concise definition is difficult and subjective, but I like how Ben Milton has characterized it:

“The more of the following a campaign has, the more old school it is: high lethality, an open world, a lack of pre-written plot, an emphasis on creative problem solving, an exploration-centered reward system (usually XP for treasure), a disregard for "encounter balance", and the use of random tables to generate world elements that surprise both players and referees. Also, a strong do-it-yourself attitude and a willingness to share your work and use the creativity of others in your game.”

These elements were present in the early years of the hobby. At first, they may seem crude, cumbersome, or outdated. But many players find the kind of gameplay that emerges from these elements very compelling. They can invoke creativity, engagement, wonder, and a sense of accomplishment.

The appreciation of old school style play shouldn’t be misconstrued as chasing a sense of nostalgia. That can be true for some, but an increasing number of players who enjoy this style weren’t alive when these rules were popular. And beyond simple retro-clone and re-hash rulesets, there are many elegant modernizations, reinterpretations, and entirely new and innovative rulesets that foster this style of play, and even more that permit it.

For links to suggested games and more introductory material, visit lithyscaphe.blogspot.com/p/principia-apocrypha.html 

## Using the principles 
It may be helpful to think of these principles as rules that you as a GM follow, supplementary to the written game rules. When you aren't sure what to do in an uncertain situation, think back to them. They can help guide the game to support old school style play.

It should go without saying that they won't all apply equally to your game or a certain adventure, depending on its style, tone, scope, and so on. They are written in a commanding style to be memorable, but take them with a grain of salt and feel free to ignore or break the ones that don’t work for your table.

When approaching a ruleset with a certain play style in mind, it’s likely that aspects of both will fight each other. But don’t agonize over gaming philosophy before actually running a game. Every GM and every group of players is so different that you will need to try them out in practice. Ask your players how they feel about the experience, and make house rules, or change systems. Play is more of a messy, amorphous act than a solid piece of art to be perfected.

## The OSR & the Cardinal Principle
The “Old School Renaissance”, or OSR, is a scene and community of people who enjoy the old school style of play. As with any other community, it's composed of all kinds of folks sharing ideas with one another. Inevitably, some of those ideas will be strongly-held opinions. Some people have louder voices, and some are deliberately provocative. Drama, miscommunication, conflict and harassment can and will happen. And as with any other community, it's up to all of its members to foster and maintain a healthy environment, for themselves and newcomers.

But when it comes down to it, remember this Cardinal Principle:

## Your Table is Yours
How you and your friends play games is not magically dictated by the opinions of others. No one gets to tell you and your players how to have fun. Find out what works for your table.

I imagine that if someone says you’re doing things wrong, they mean well. They want to help you have the same kind of experiences that they find fun. But there are any number of hidden variables between their experience and yours. The principles are partially meant to help disambiguate what people mean by “OSR” and old school style play, and clarify the reasoning behind certain rules and methods.

That said, the OSR is full of incredibly creative folks and an abundance of unique and engaging content for inspiration or direct use in your games. If you’re not a part of it already, please, take this as an invitation to join the conversation and share your own ideas! The OSR thrives on new perspectives and pushing the boundaries of what can be done with these games.

In other words, the OSR scene is not a set of rules to follow, it’s an ever-growing “Appendix N”.

—David Perry


# Be an Impartial Arbiter

## Rulings Over Rules
(P) The primeval old school principle. Old school style games are often sparse in what situations their rules cover. There are often minimal, or no "skills" or "feats". This is a feature, not a bug. The flexibility this openness allows is a big part of the appeal of old school style games. Let players take advantage of this openness and try crazy things (and apply logic to taste).

(P) When you encounter a situation that the rules don't seem to cover, don't get distracted searching for it. Instead, make a common-sense ruling within the spirit of the game and move on. It doesn’t have to be perfect. Later, if you find there is in fact no rule and it could come up again, make a note of the ruling and apply it consistently.

## Divest Yourself of Their Fate
(P) As GM (or Judge or Referee, perhaps more appropriate for old school style games), you are not an antagonist to the players or characters. Nor are you an author writing their story. (P)ortray the world and embody its denizens genuinely, as they would react to the characters’ behavior. Don’t set out to tell a story, let one emerge from the characters’ interactions with the world.

(M) Be fair and impartial. Do not fudge rolls and avoid rolling in secret. This keeps the game honest and dangerous, and prevents any accusations of favoritism or railroading. It also encourages the players to manipulate and engage with the fictional world, rather than with the GM.

## Leave Preparation Flexible
(M) Don’t prepare a plot for the players to follow. During the game, observe how the players deal with a situation, and extrapolate the effects of their actions based on what you know. Don’t plan the results too far ahead of time; players rarely do what you expect them to.

(M) Don’t overdo the preparation! Keep your situation ideas loose enough that they can be adapted to the PC’s choices and the flow of the game. Remember that unused prep can always be recycled in later sessions. After each session, ask the players what they plan on doing next and prep a few situations related to that. The direction of the game should be guided by the player’s decisions, not the GM’s.

(P) Some RPG products are more like toolkits than sourcebooks, helping you adapt and build a unique, flexible setting around your players, rather than being restricted to a rigid, dictated instance of someone else’s.

## Build Responsive Situations
(P) Establish situations with several actors or factions pursuing their own ends. Let the players’ actions affect this environment, and let the consequences affect the players in turn. Show the situations worsening if the players don’t address them.

(M) One way to create interesting situations is to draw a grid that maps the relationships between the elements of a situation and how they relate and interact, or how the party might intervene.

## Embrace Chaos...
(P) Listen to that capricious muse, the dice. Relying only on your own imagination can become exhausting and predictable, and can feel less like a concrete world to the players. And external inspiration and random results help you divest yourself of their fate. 

(M) Use random tables to keep the game fresh. The surprising twists that random tables add can bring an energy and mystery to the game that is hard to improvise.

## ...But Uphold Logic
(P) But don’t use random results to the extent that the world feels nonsensical. If there is an obvious choice, consequence, or cause, use it. This can help maintain verisimilitude and let players make reasonable plans. It also emphasizes the surprise and intrigue of the instances of randomness when you do use them. You might also customize random tables to give a more cohesive feel to your game’s setting.

## Let Them Off the Rails
(P) Feel free to let them know where most of your prep is, but if you expect them to zig and they zag, don’t constrain or re-route them. If you don’t know what comes next, let random tables fill in the blanks, then build on it between sessions. Find the excitement they see and embrace, too, the chaos of the players.


# Get Them Thinking

## XP for Discovery and Adversity
(P) Many old school style games directly correlate XP earned with the value of treasure returned to safety. This is a convenient abstraction of the characters learning from the exploration and adversity of retrieving that treasure.

(P) XP may also be granted for defeating monsters, but that doesn't necessarily mean killing them. And the XP value is usually much less than that of the treasure they guard. This incentivizes players to out-think monsters instead of defaulting to violence.

(P) XP-for-treasure is like the fuel of the game's engine. Player decisions almost always hinge upon it. It's also an effective control knob. By adjusting the amount of treasure available, you can control the rate of PCs leveling. And you can tailor the focus of your game by adding value to exploring wilderness, saving prisoners, acquiring books, artisanal brewing ingredients, crystallized memories...

## Player Ingenuity Over Character Ability
(M) Old school PCs are very minimalistic because the character sheet is mostly there for when players make a mistake.

Players are not meant to solve problems with die rolls, but with their own ingenuity. Present them with problems that don’t require obscure knowledge, have no simple solution, but have many difficult solutions.

(M) Examples: Cross a moat of crocodiles. A door in a deep dungeon only opens in sunlight. A key in a lake of acid.

## Cleverness Rewarded, Not Thwarted
(M) Be generous with clever solutions to a problem, as long as they are within the realm of possibility. If the action is unlikely or dangerous, call for a save or ability check, but only forbid a creative solution if it is clearly impossible.

(M) If players tend not to think this way, present them with situations that are nearly impossible to tackle head on, and strongly reward even slightly creative solutions. One of your goals as a GM is to encourage this mentality. Feel free to tell your players that cleverness will get them farther than brute force.

(P) Give them the benefit of the doubt when they’ve worked to give themselves the upper hand in the fiction. Don’t shy away from giving mechanical advantage.

## Ask Them How They Do It
(P) Encourage players to interrogate the fiction of the environment “manually”, asking them to describe the manner in which they interact, rather than eliding their actions via a roll or assumed character ability. You can always grant them a roll for a discovery or insight if they give up.

## Let Them Manipulate The World
(M) The focus of the game should be on creative problem solving, not brute force, so give players tools to make that appealing. When you give players tools, you give them new ways to engage with the world. Add elements that allow the players to bend the world to their will.

(M) For example: rival factions to manipulate, potions with weirdly specific effects, items that can be combined or repurposed, dungeons with shortcuts and back passages.

## Good Items Are Unique Tools
(M) A good tool doesn’t (only) increase PCs’ damage or add an ability bonus; it does an odd, very specific thing that is only powerful when used cleverly. This turns every problem into a puzzle and encourages creative solutions.

(M) Examples: A rope that becomes as rigid as steel on command. A coin that lands on any result you wish when flipped. A bell that produces silence.

(P) Just don’t let magic items become superpowers that trivialize every challenge. Give them a downside, an interesting cost, or a chance to deplete each time it is used.

## Don’t Mind The Fourth Wall
(P) Don’t worry much about “metagaming”, or the dissonance
between what the players know and what the characters know. Favor the
ingenuity of the players over strict personification of their characters.


# Build Rocks & Hard Places

## Offer Tough Choices
(M) Make the players weigh risk versus reward. The deeper players go into the wilderness or dungeon, the more perilous things should become. Whether because resources are running low (food, health, equipment, light, etc.) or because danger builds the longer they linger, keep the players asking if it is worth pushing their luck just a little bit farther. The greatest treasures are always the hardest to reach.

(M) Risk and reward are also at the heart of combat. The PCs’ low health is meant to push combat quickly toward the point where players ask themselves, “Should I retreat to fight another day, or do I risk it all to finish them now?” The thrill of that choice is at the heart of combat.

(M) Look for situations where all obvious choices come with a heavy cost. These situations encourage unorthodox solutions and lateral thinking.

## Build Challenges With Multiple Answers...
(P) Avoid singular chokepoints to progress. Give them obvious, equally, but differently-difficult alternatives. You can also keep an extra option in your pocket that they have to dig for. Maybe it’s obscure, but preferable. Maybe it’s just as difficult, but more beneficial.

(L) "There's a magically locked iron gate the players have to get past... how could they? I guess one of the NPCs has a key... and there's a potion of Eat Metal hidden in room 12C." When you build your adventures, seed them with challenges that you know the answer to. Maybe the player characters have a core capability to get past the challenge, or maybe you've just placed the solution somewhere else for them to find. Use these to encourage players to dig into the fiction, and explore. If a challenge is critical for the continuation of the adventure, consider placing a few solutions. Three is a good number.

## ...And Challenges With No Answer
(P) Trust in your players. Let them surprise you and find answers to problems that you couldn’t expect, and can’t help them with. These can be the most rewarding challenges.

(L) "The deeps are stalked by a living maelstrom of ravenous psychic energy. If the players want to get the Golden Falcon they'll have to get past it, but I have no idea how they'll manage that." These exist to force players to be creative in ways that surprise everyone at the table. Be cautious with placing these as challenges critical for the continuation of the adventure (unless you intend for players to retreat and come back later), but sprinkling them around can surprise everyone at your table.

## Subvert Their Expectations
(P) It’s inevitable that players will have knowledge about common fantasy elements from pop culture. Inject common monsters, locations, and situations with your own unique twists; surprise them. This encourages players to explore these differences and solve new problems they aren’t familiar with.


# Dice With Death

## Deadly But Avoidable Combat
(M) Combat in old school RPGs is often neither balanced nor fair, and PCs should encounter foes far more powerful and numerous than they are. Players should learn to treat combat like real-world warfare and use ingenuity, preparation and underhanded tactics to rig the results in their favor. Encourage the players to outsmart and out-plan their enemies if they want to survive.

(P) Old school dungeons are usually not about being “cleared” of enemies. Dungeons constrain and focus possible actions. This makes it easier for both the players and the GM to identify, reason about, and plan around the problems presented. Potentially deadly combat is a common problem, one which should encourage solving in a variety of ways other than head-on.

## Keep Up The Pressure
(M) Whether it’s through random encounter rolls when time passes, or because the dungeon is filling with sand, or because a PC will die in 10 turns from poison, keep the players desperate and on a clock. Maintain a tension between the desire to explore and loot, and the terror of remaining too long.

(P) If the players repeat attempts at a challenge, such as a lock-picking check, give them a consequence, such as a chance for wandering monsters. Do be sure to let them breathe; back in town, on long trips through the wilderness, or if they find a safe dungeon room. But always consider a chance for trouble, be it 1-in-6 per hour, day, week, or otherwise.

## Let The Dice Kill Them...
(L) Remember, the G(M) is not an antagonist to the players, but their survival is on them.

(M) If the rules and dice say that someone is dead, they’re dead. Protecting the PCs from death can result in games that lack tension, and players who only solve problems with brute force.

(P) Unambiguous character death gives weight to both the risks and rewards of play. Character creation is simple and quick in these games for a reason, and if they have hirelings or retainers, they have instant replacement characters. And don’t worry about players not feeling attached to "simple" characters; they will after gaining something to lose.

## ...But Telegraph Lethality
(M) Give players the chance to think their way around threats and obstacles by telegraphing them ahead of time. No one likes their death to be random chance. When a PC dies, it should be their fault.

(P) Or at least, they should know why.


# Be Their World

## Reveal The Situation
(M) Don’t hide important information from the players. If the PC could reasonably know something, tell the player and move on. The game is about making decisions, and players can’t make good decisions without good information.

(L) The players will be probing your vision of this place for useful information. Put your mind into that world, explore, and bring back what's valuable. Likewise, apply a real-world logic to populations and challenges, rather than building a carefully balanced sequence of fights.

(P) Assume the characters have common sense, and mention potential danger when it becomes obvious. Don’t make them plummet from a 50 foot cliff because the player said they would “hop down” it; they may have heard “five” instead of “fifty”.

## Give Them Layers To Peel
(L) What are the PCs aware of already? What do they notice at their first glance? Which of those "first glance" things hides information revealed on closer inspection? How would players get that information? What's obvious, what's subtle, what's hidden, and what's invisible? Create layers of information for the players to peel back and explore.

(P) Some adventure modules indicate what would be obvious to a character to help you parse these layers. If that’s not the case, take care not to blurt out the secrets in a list of the contents of a room.

## Don’t Bury The Lead
(L) Keep details of your world gameable. Players should be able to act on the information you're telling them: "Her eyes are a shifting mottled green" helps players remember the NPC, sure–but "...and you notice she never stands more than one long step away from the table and its contents" gives them information they can act on. "The pillars are ornately carved marble... the furthest one is crossed with a latticework of cracks." Your details should allow players to make informed decisions and take effective action. You can hide these details within your layered environments for players to discover, but remember to make them matter.

(P) Let unknown mysteries become known problems. Maintaining secret lore can be fun, but until it's a problem for the players, it doesn't exist. Tie lore and mysteries into treasure that the players are already want to acquire.

## NPCs Aren't Scripts
(P) Give NPCs a motivation or concern that doesn’t involve the PCs. This lends them some depth and grounding, as well as giving the PCs a lever to pull, if they can find it.

(M) Treat NPCs like real people. Think about what NPCs want, especially in combat. NPCs want to stay alive, and will rarely start fights that they don’t have a high chance of winning. Only fanatical NPCs will fight to the death; most will try to retreat or surrender if they are losing. Also, remember that enemies and allies can be made to switch sides with the right motivation.

(P) Use Reaction Rolls and Morale Rolls liberally. Reaction Rolls provide more variety to encounters, and demonstrate that combat isn't always assumed. Morale Rolls encourage battles to end before becoming a slog, or a slaughter.

## Keep The World Alive
(M) Old school RPGs shine with improvisation and extrapolation, not rigid plots. During the game and in between sessions, think about how the other characters and factions would respond to what the PCs are doing, and develop them accordingly. Your guiding principle should be “What are the logical consequences?" 

(M) Give the players a stake in the world. As the game goes on, players may accumulate a lot of money from completing jobs and looting treasures. Encourage them to use this money to buy property, hire retainers, or found factions. This can open up new ways for the players to interact with the world and affect its history.


# Old School Principles for Players

## Learn When To Run
(L) Old school adventures often present deadly encounters that, to the eye of a modern gamer, may seem like you’re expected to beat them. Learn to dig into the fiction to see the relative power of what you're facing, and don't be afraid to cut your losses. A party that drags away one dead body is a party on their way to a Cleric, instead of on their way through a monster's digestive system.

## Combat As War, Not Sport
(P) Don’t expect encounters to be “balanced”. Approach combat with as much trepidation and preparation as you would in real life. Nor are encounters self-contained. Think outside the box, outside the encounter area, outside the dungeon. Think like Sun Tzu. Think laterally or die.

## Don’t Be Limited By Your Character Sheet
(P) Rules and mechanics are only triggered by what happens as established in the conversational fiction of play. To do something, describe your character doing it; if you need to roll dice, the GM will let you know.

(P) When presented with a problem, don't expect to “use” your character’s skills or abilities on it; investigate it by asking the GM questions and describing what your character tries.

(P) Don’t worry much about low stats, or roleplaying to match them. If they’re low, it just means you’ll have to be clever, gather information, and plan ahead to avoid dangerous rolls! Or forge ahead, foolhardy, and look forward to rolling up a new character.

## Live Your Backstory
(P) Don’t put much work into a backstory for your characters. Their experiences in play will be more real to you and your friends than anything you write. An early death won’t sting quite as much, and a survivor will have real tales to tell, and experience to take pride in.

## Power Is Earned, Heroism Proven
(P) Unlike many modern RPGs, your character starts with little power. Your meager means and abilities at first level encourage lateral thinking to get you out of trouble. Rising to a challenge (or fleeing it) means more when their life is on the line.

(P) Likewise, if you wish to play a true hero, don’t expect anyone to salute you when you first ride into town. Prove your heroism through your character’s actions.

## Scrutinize The World
(L) Discard any assumptions about other fantasy worlds, and be curious about the one you’re playing in. Pay attention to details—about characters, the environment, social situations, and more. Take notes on them! Make maps of them! Information is leverage, my crafty friend. Those details can save your life.

## Interrogate The Fiction
(L) If you were in a room with a heavy vase in one corner, and you wanted to know what was behind it, what would you do? Probably drag it to the side, right? Looking for an air current? Lick a finger and hold it up. Judging the slope of a floor? Spill a little water on the ground. Engage the fiction of the game world as real. Describe the real actions you take to achieve the effect you're looking for. Remember, other games may have dice rolls to do this for you — many old school games don't, so engage!

## The Only Dead End Is Death
(L) That dead-end hallway may hide a secret door, or maybe there's another passage to investigate. The gargantuan monstrosity in the courtyard? Maybe you can get around it, or negotiate. A recalcitrant noble? Maybe someone knows how to get some leverage. Couldn't pick that iron door? Maybe one of those unidentified potions will help. Old school games have lots of hard blockers. When your first attempt fails, change tactics—the dead end is just the beginning of your solution. Often, digging into the fiction and engaging the world as real will open up new and unexpected avenues.

## Let Your Creativity Flow
(L) Your class and/or race can do some unique things the other folks can't. Learn to recognize when it's your turn to shine, and when it's someone else's. When it's your turn, really go for it. Outside of the game mechanics of your character, what are your unique inspirations and ideas? Do you see a clever use for a magic item? Do you want to try negotiating with the ferocious monster? Do you see a weakness in the defenses the others don't immediately recognize? Could you combine a few of these opportunities in a unique way? Open up your brain, and let in the weird and the creative. The world is so bizarre... it just might work.

## Play To Win, Savor Loss
(L) Everyone wants to succeed, and certainly everyone wants to play with friends they feel are aiming to succeed —but that may not always happen. Your characters may get turned into frog-people, lose limbs, be stricken by leprosy, turned into stone, cursed to burp up slugs, entombed in the earth for 10,000 years, or just die from being stabbed in the gut by a farmer with a pitchfork. Learn to love the disgusting, horrifying, shocking, surprising, and even disappointing ways your characters are set back.

(P) And remember, through play, a story emerges larger than any one character. You will make your mark on the world, be it an unknowingly misleading arrow scratched into a dungeon wall, or a crater where a city once stood.

